# TeamCity template for BastilleBSD

Template for [BastilleBSD](https://bastillebsd.org/) to run a
[TeamCity](https://www.jetbrains.com/teamcity/) service inside of a
[FreeBSD](https://www.freebsd.org/) jail.

By default the hard coded version of TeamCity (see
[Bastillefile](Bastillefile)) will be installed using default settings.

**Please note that you _have to_ define an external directory which will be
mounted inside the jail as data store for the TeamCity installation and data
directory.

## License

This program is distributed under 3-Clause BSD license. See the file
[LICENSE](LICENSE) for details.

## Bootstrap

So far bastille only supports downloading from GitHub or GitLab, so you have
to fetch the template manually:

```
# mkdir <your-bastille-template-dir>/wegtam
# git -C <your-bastille-template-dir>/wegtam clone https://codeberg.org/wegtam/bastille-teamcity.git
```

## Usage

### 1. Install the default version into a jail

```
# bastille template TARGET wegtam/bastille-teamcity --arg EXT_DIR=/srv/tc
```

### 2. Install with custom settings

```
# bastille template TARGET wegtam/bastille-teamcity --arg EXT_DIR=/srv/tc --arg VERSION=2021.1.4 --arg DIR=/srv/teamcity
```

For more options take a look at the [Bastillefile](Bastillefile).

